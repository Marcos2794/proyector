library(readxl)
library(dplyr)
library(lubridate)
library(feather)
library(lucr)
library(DT) 

##Conectar con SQL Server
library(RODBC)
sqlconn<- odbcConnect("proyecto")


ventas_dw<- sqlQuery(sqlconn, "select p.Producto,
		 c.ID_CLIENTE,
         suc.Nombre,
		 e.EMPLEADO,
         t.FECHA,
		 v.CANTIDAD,
         v.MONTO
   from FARMACIASA.dbo.ventas v
   inner join FARMACIADW.dbo.dim_clientes c
      on v.id_cliente = c.codigo_cliente
   inner join FARMACIADW.dbo.dim_empleados e
      on v.ID_EMPLEADO = e.CODIGO_EMPLEADO
   inner join FARMACIADW.dbo.DIM_Sucursal suc
      on v.ID_SUCURSAL = suc.codigo_sucursal
   inner join FARMACIADW.dbo.dim_producto p
      on v.id_producto = p.codigo_producto
   inner join FARMACIADW.dbo.dim_tiempo t
      on cast(v.fecha as date) = t.fecha")

head(ventas_dw,6) 

colones <- function(x) {
   to_currency(x, currency_symbol = "CRC ",
               symbol_first = TRUE, group_size = 9,
               group_delim = ".", decimal_size = 2,
               decimal_delim = ",")
}

#multiplicar<-function(val){
# dato<- val*10
#return(dato)
#}

clientes<-ventas_dw %>% group_by(ID_CLIENTE) %>% summarise(TOTAL=n())

class(ventas_dw$FECHA)
ventas_dw$FECHA<-as.Date(ventas_dw$FECHA)
summary(ventas_dw)

data_venta_sucursal<- ventas_dw %>% group_by(Nombre,Producto) %>% select(c("Nombre", "Producto", "MONTO")) %>% summarise(TOTAL=sum(MONTO))
names(data_venta_sucursal)

head(data_venta_sucursal,6) 


# Sucursales con mayores numeros de ventas
sucursales_top5 <- ventas_dw %>%
   filter(!is.na(Nombre)) %>% 
   group_by(Nombre) %>%
   summarise(
      total = n()
   ) %>%
   arrange(desc(total)) %>%
   head(5) %>% 
   select(Nombre) %>% pull()

venta_mensuales<- ventas_dw %>%
   #los datos no salen para todas las sucursales solo para las TOP 5 del selector
   group_by(Nombre,Producto,year(FECHA)) %>%filter(Nombre %in% sucursales_top5 ) %>% 
   summarise(TOTAL=sum(MONTO))
colnames(venta_mensuales)<-c("Nombre","Producto","ANNIO","MONTO")
venta_mensuales<-as.data.frame(venta_mensuales)
venta_mensuales$ANNIO<-as.factor(venta_mensuales$ANNIO)


ventas_fechas<-ventas_dw %>%
   #los datos no salen para todas las sucursales solo para las TOP 5 del selector
   group_by(Nombre,Producto,FECHA ) %>%select(c("Nombre", "Producto","FECHA","MONTO")) %>%
   filter(Nombre %in% sucursales_top5) %>% summarise(TOTAL=sum(MONTO))


####################compras#############################

sqlconn<- odbcConnect("proyecto")


compras_dw<- sqlQuery(sqlconn,"select comp.ID_PEDIDO,
                                 prov.Proveedor,
                                 suc.Nombre,
                                 p.Producto,
                                 comp.CANTIDAD,
                                 t.FECHA,
                                 comp.PRECIO_COMPRA
                                 from FARMACIASA.dbo.COMPRAS comp
                                 inner join FARMACIADW.dbo.DIM_Proveedor prov
                                 on comp.ID_PROVEDOR = prov.Codigo_Proveedor
                                 inner join FARMACIADW.dbo.DIM_Sucursal suc
                                 on comp.ID_SUCURSAL = suc.codigo_sucursal
                                 inner join FARMACIADW.dbo.dim_producto p
                                 on comp.id_producto = p.codigo_producto
                                 inner join FARMACIADW.dbo.dim_tiempo t
                                 on cast(comp.fecha as date) = t.fecha")

head(compras_dw,6) 

colones <- function(x) {
   to_currency(x, currency_symbol = "CRC ",
               symbol_first = TRUE, group_size = 9,
               group_delim = ".", decimal_size = 2,
               decimal_delim = ",")
}

clientesc<-compras_dw %>% group_by(Proveedor) %>% summarise(TOTAL=n())

summary(compras_dw)


data_compra_sucursal<- compras_dw %>% group_by(Proveedor,Nombre,Producto, CANTIDAD, PRECIO_COMPRA) %>% select(c("Proveedor", "Nombre", "Producto", "CANTIDAD", "PRECIO_COMPRA")) %>% summarise(TOTAL=sum(CANTIDAD))
names(data_compra_sucursal)

head(data_compra_sucursal,6) 


# pedidos por provedor
provedor_top <- compras_dw %>%
   filter(!is.na(Proveedor)) %>% 
   group_by(Proveedor) %>%
   summarise(
      total = n()
   ) %>%
   arrange(desc(total)) %>%
   head(5) %>% 
   select(Proveedor) %>% pull()

compras_mensuales<- compras_dw %>%
group_by(Nombre,Producto,year(FECHA)) %>%filter(Proveedor %in% provedor_top ) %>% 
summarise(TOTAL=sum(CANTIDAD))
colnames(compras_mensuales)<-c("Proveedor","CANTIDAD","ANNIO","MONTO")

compras_mensuales<-as.data.frame(compras_mensuales)
compras_mensuales$ANNIO<-as.factor(compras_mensuales$ANNIO)


summarise(compras_mensuales)
names(compras_mensuales)


compras_fechas<-compras_dw %>%
   #los datos no salen para todas las sucursales solo para las TOP 5 del selector
   group_by(Proveedor,CANTIDAD ) %>%select(c("Proveedor", "CANTIDAD")) %>%
   filter(Proveedor %in% provedor_top) %>% summarise(TOTAL=sum(CANTIDAD))


