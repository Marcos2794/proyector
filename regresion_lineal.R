library(RODBC)

sqlconn<- odbcConnect("proyecto")


ventas_dw<- sqlQuery(sqlconn, "select p.Producto,
		 c.ID_CLIENTE,
         suc.Nombre,
		 e.EMPLEADO,
         t.FECHA,
		 v.CANTIDAD,
         v.MONTO
   from FARMACIASA.dbo.ventas v
   inner join FARMACIADW.dbo.dim_clientes c
      on v.id_cliente = c.codigo_cliente
   inner join FARMACIADW.dbo.dim_empleados e
      on v.ID_EMPLEADO = e.CODIGO_EMPLEADO
   inner join FARMACIADW.dbo.DIM_Sucursal suc
      on v.ID_SUCURSAL = suc.codigo_sucursal
   inner join FARMACIADW.dbo.dim_producto p
      on v.id_producto = p.codigo_producto
   inner join FARMACIADW.dbo.dim_tiempo t
      on cast(v.fecha as date) = t.fecha")

View(ventas_dw)

datos<-ventas_dw [,6:7] # fila ,columna
head(datos)

# los valores disparados es porque no se realizo un debido tratamiento de los datos iniciales
# por eso se ven la amplitos entre los datos de nivel de confianza

# practicamos cambio de nombres
colnames(datos)<-c("Cantidad","Monto")
names(datos)

boxplot(datos$Cantidad) # se genera un grafico para ver valores atipicos
boxplot(datos$Cantidad,horizontal = T) # es lo mismo pero horizontal

boxplot(datos$Monto)

hist(datos$Monto,col = rainbow(3),main = "Histograma de Monto") # se crea un histograma de datos
density(datos$Monto)
hist(datos$Cantidad)


#Tratamiento de Outliers
#Eliminar las filas 
library(dplyr)

datos_so1<-datos %>% filter(Monto<80000)
boxplot(datos_so1$Monto)
boxplot(datos_so1$Cantidad)

datos_so2<-datos_so1 %>% filter(Cantidad>=4)
boxplot(datos_so2$Monto)
boxplot(datos_so2$Cantida)

datos<-datos_so2

# Segunda forma eliminar outliers
impute_outliers <- function(x, removeNA = TRUE){
  quantiles <- quantile(x, c(0.05, 0.90), na.rm = removeNA)
  x[x<quantiles[1]] <- mean(x, na.rm = removeNA)
  x[x>quantiles[2]] <- median(x, na.rm = removeNA)
  x
}

datos$cantidad_imput<-impute_outliers(datos$Cantidad)
datos$monto_imput<-impute_outliers(datos$Monto)
boxplot(datos$monto_imput)
View(datos)


#vemos las correlaciones con pairs
plot(datos)

pairs(datos, main = "Correlacion de los datos",
      panel=panel.smooth, cex = 2,
      pch = 20, bg="blue",
      cex.labels = 1, font.labels=1)

#vemos la matriz de correlaciones simples
cor(datos)
cor(datos$Monto,datos$Cantidad)
library(corrplot)
coor<-cor(datos)
corrplot(coor,method = "circle")

# vemos las correlaciones  parciales
library(Rcmdr)
# para ver las variables mas importantes
partial.cor(datos)


#El modelo se hace con variables que tengan correlacion
#simple
lm_ventas<-lm(data = datos,Monto~Cantidad)
#Resumen del Modelo
summary(lm_ventas)
# vemos que tanto esta el error
coeficiente_variacion<-100*(261400/mean(datos$Monto))

#p-value dice que si hay modelo es mayor a 0 hipotesis anova
# p-value: < 2.2e-16
#Indica que porcetage de bueno es el modelo
# Multiple R-squared:

# graficamos los datos del modelo

#ploteamos el modelo
plot (datos$Cantidad,datos$Monto,xlab = "Cantidad",ylab = "monto de las ventas",
      main="datos del modelo lineal") 

# los * simbolizan la significancia de las variables
#el porcentage de acierto esta en Multiple R-squared

#predecimos el valor para una superficie de 1500 metros con el modelo entrenado
#para una cada de 1500 metros en una regresion lineal simple
predict(lm_ventas,data.frame(Cantidad=15)) 


# valoro si es nesesario eliminar variables del modelo
# el AIC debe ser el menor , si elimino variables el AIC debe ser menor para poder considerar eliminar
# variables en este ejemplo banos se puede eliminar porque casi no afecta pero superficie no
step(lm_ventas)
step(lm_ventas)

# obtener el limite inferior y superior del modelo 
predict(lm_ventas,data.frame(Cantidad=3,Monto=95000),level = 0.95,interval = "prediction") 

#generalos los dataframen para el resultado final

names(datos)

#Creamos una funcion para agregar el mensaje al resultado final
fmensaje <- function(valor){
  sapply(valor, function(x) if(x < 0) paste(x,"debajo de valor predico ") else 
    paste (x,"por encima del valor predicho"))
}

resultado<-data.frame(datos$Cantidad,datos$Monto,lm_ventas$fitted.values,
                      round(datos$Monto-lm_ventas$fitted.values),
                      fmensaje(round(datos$Monto-lm_ventas$fitted.values))
)

names(resultado)<-c("Cantidad","Monto","PrecioModelo","Diferencia","Descripcion")
View(resultado)
